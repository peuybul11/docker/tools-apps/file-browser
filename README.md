# File Browser

Ubah **/path/hdd1:/data/hdd1**
Sesuaikan dengan file atau storage sesuai existing  

Create Docker Network
```
docker network create local-network
```

Running Docker
```
docker-compose up --build -d
```

Default Credential Username & Password
```
admin
```